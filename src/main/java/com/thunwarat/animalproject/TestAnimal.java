/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.animalproject;

/**
 *
 * @author ACER
 */
public class TestAnimal {
    public static void main(String[] args) {
        Snake snake = new Snake("Keaw Keaw");
        snake.crawl();
        snake.eat();
        snake.walk();
        snake.speak();
        snake.sleep();
        
        System.out.println("-------------------------------");
        
        Alligator alligator = new Alligator("ChaRaWan");
        alligator.crawl();
        alligator.eat();
        alligator.walk();
        alligator.speak();
        alligator.sleep();
        
        System.out.println("-------------------------------");
        
        Cat cat = new Cat("Tod Tod");
        cat.run();
        cat.eat();
        cat.walk();
        cat.speak();
        cat.sleep();
        
        System.out.println("-------------------------------");
        
        Dog dog = new Dog("Boo Boo");
        dog.run();
        dog.eat();
        dog.walk();
        dog.speak();
        dog.sleep();
        
        System.out.println("-------------------------------");
        
        Human human = new Human("Baanjob PoobThorn");
        human.run();
        human.eat();
        human.walk();
        human.speak();
        human.sleep();
        
        System.out.println("-------------------------------");
        
        Crab crab = new Crab("Taraba");
        crab.swim();
        crab.eat();
        crab.walk();
        crab.speak();
        crab.sleep();
        
        System.out.println("-------------------------------");
        
        Fish fish = new Fish("Tong Tong");
        fish.swim();
        fish.eat();
        fish.walk();
        fish.speak();
        fish.sleep();
        
        System.out.println("-------------------------------");
        
        Bat bat = new Bat("Mr.BatMan");
        bat.fly();
        bat.eat();
        bat.walk();
        bat.speak();
        bat.sleep();
        
        System.out.println("-------------------------------");
        
        Bird bird = new Bird("Nok Nok");
        bird.fly();
        bird.eat();
        bird.walk();
        bird.speak();
        bird.sleep();
        
        System.out.println("-------------------------------");
    }
}
