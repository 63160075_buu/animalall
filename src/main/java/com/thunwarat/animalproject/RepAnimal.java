/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.animalproject;

/**
 *
 * @author ACER
 */
public abstract class RepAnimal extends Animal{
    
    public RepAnimal(String name, int numberOfLeg){
        super(name, numberOfLeg);
    }
    
    public abstract void crawl();
}
